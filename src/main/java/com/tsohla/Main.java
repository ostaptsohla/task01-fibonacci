package com.tsohla;

import java.util.Scanner;

public class Main {

    private static boolean isEven(final int number) {
        if (number % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    private static void printOddNumbers(int beginningOfInterval, int endOfInterval) {
        for(int i = beginningOfInterval;i <= endOfInterval; i++) {
            if(!isEven(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    private static void printEvenNumbers(int beginningOfInterval, int endOfInterval) {
        for(int i = endOfInterval;i >= beginningOfInterval; i--) {
            if(isEven(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    private static void printSumOfOddNumbers(int beginningOfInterval, int endOfInterval) {
        int sum = 0;
        for(int i = beginningOfInterval;i <= endOfInterval; i++) {
            if(!isEven(i)) {
                sum += i;
            }
        }
        System.out.println("Sum of odd numbers:" + sum);
    }

    private static void printSumOfEvenNumbers(int beginningOfInterval, int endOfInterval) {
        int sum = 0;
        for(int i = beginningOfInterval;i <= endOfInterval; i++) {
            if(isEven(i)) {
                sum += i;
            }
        }
        System.out.println("Sum of even numbers:" + sum);
    }

    private static void printFibonacciNumbers(int f1, int f2, int sizeOfSet) {
        int[] fibonacci = new int[sizeOfSet];
        int oddNumbers = 0;
        int evenNumbers = 0;
        fibonacci[0] = f1;
        fibonacci[1] = f2;
        for(int i = 2; i < sizeOfSet; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }
        for(int i : fibonacci) {
            System.out.print(i + " ");
            if(isEven(i)) {
                evenNumbers++;
            } else {
                oddNumbers++;
            }
        }
        System.out.println("\nPercentage of odd Fibonacci numbers:" + (double)oddNumbers / sizeOfSet);
        System.out.println("Percentage of even Fibonacci numbers:" + (double)evenNumbers / sizeOfSet);
    }

    private static int biggestOddNumber(int endOfInterval) {
        if(!isEven(endOfInterval)) {
            return endOfInterval;
        } else {
            return endOfInterval - 1;
        }
    }

    private static int biggestEvenNumber(int endOfInterval) {
        if(isEven(endOfInterval)) {
            return endOfInterval;
        } else {
            return endOfInterval - 1;
        }
    }

    private static void printMenu() {
        boolean flag = true;
        int sizeOfSet;
        int beginningOfInterval;
        int endOfInterval;
        int action;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter first number of interval:");
        beginningOfInterval = scan.nextInt();
        System.out.println("Enter last number of interval:");
        endOfInterval = scan.nextInt();
        while(flag) {
            System.out.println("Choose action:");
            System.out.println("1.Print odd numbers in ascending order.");
            System.out.println("2.Print even numbers in descending order.");
            System.out.println("3.Print the sum of odd numbers.");
            System.out.println("4.Print the sum of even numbers.");
            System.out.println("5.Fibonacci numbers.");
            System.out.println("0.Close program.");
            System.out.println("Enter number of action:");
            action = scan.nextInt();
            switch (action) {
                case 1:
                    printOddNumbers(beginningOfInterval, endOfInterval);
                    break;
                case 2:
                    printEvenNumbers(beginningOfInterval, endOfInterval);
                    break;
                case 3:
                    printSumOfOddNumbers(beginningOfInterval, endOfInterval);
                    break;
                case 4:
                    printSumOfEvenNumbers(beginningOfInterval, endOfInterval);
                    break;
                case 5:
                    System.out.println("Enter the size of set");
                    sizeOfSet = scan.nextInt();
                    printFibonacciNumbers(biggestOddNumber(endOfInterval),biggestEvenNumber(endOfInterval),sizeOfSet);
                    break;
                case 0:
                    flag = false;
                    break;
            }
        }
    }

    public static void main(String[] args) {
        printMenu();
    }
}
